#    Atividades de Hoje 08/08/2022
# 1. Crie um programa que escreva "Olá, Mundo!"na tela.
# 2. Faça um programa que leia o nome de uma pessoas e mostre uma mensagem de boas-vindas.
# 3. Crie um programa que leia dois números e mostre a soma entre eles.


print("==============================================================================")

# RESP. DA ATV-01
print("Olá, Mundo\n")

print("===============================================================================")

# RESP. DA ATV-02

# Entrada
print("Qual seu nome: ")
nome = input()

# ou

#nome = input("Qual seu nome: ")
idade = int(input("Qual Sua Idade: ")) 
# O CAST(converte dados de um tipo para outro) 'int' para converter idade para um numero 'inteiro'.  

# Saida
print(f"Seja Bem Vindo {nome}")         # formato mais recente na ver-3.x
print("Seja Bem Vindo ", nome)          # formato recente (creio eu que seja da ver-3.x tambem).
print("Seja Bem Vindo %s" % nome)       # fomato antigo era da ver 2.x
print("Sja Bem Vindo {0}".format(nome)) # format recente na ver-3.x

print(f"Sua idade é  {idade}")  
print("Sua idade é ", idade)         
print("Sua idade é %s" % idade) 
print("Sua idade é {0} e seu nome é {1}\n".format(idade, nome))    

print("================================================================================")


# RESP. DA ATV-03

print("\nSOMANDO NÚMEROS\n")

# Entrada
n1 = int(input("Digite um valor: ")) 
n2 = int(input("Digite outro Valor: "))
# posso fazer o cast na mesma linha ou fora tambem ex: [ num1 = int(n1) ]

# Execução
soma_tot = (n1 + n2)

# Saída
print(f"a soma entre {n1} e {n2} e igual a {soma_tot}")
print(f"{n1} + {n2} = {soma_tot}")


print("=================================================================================")






