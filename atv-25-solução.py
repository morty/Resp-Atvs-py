"""
  25. Crie um programa que leia o nome de uma 
 pessoa e diga se ela tem "SILVA"no nome
"""

# ======================== USANDO LOOP WHILE ============================ 

nome = input("Digite seu nome completo: ")

mai = nome.upper()
nom = mai.split()

i=0
while(i < len(nom)):
    if(nom[i] == "SILVA"):
        print("Seu nome tem SILVA")
    i += 1

print(f"O seu Nome Completo e: {nome.title()}")
# ==================================================================


# =================== USANDO LOOP FOR =============================

nome = input("Digite seu nome completo: ")

mai = nome.upper() # passando todos caracteres para maiuscula
nom = mai.split() # tranformando em um array para separar cada parte do nome

for indice in nom:
    if(indice == "SILVA"): # usei maiusculo justamente para evitar erro na hora de comparar
       print("Seu nome tem SILVA")

print(f"O seu Nome Completo e: {nome.title()}")
