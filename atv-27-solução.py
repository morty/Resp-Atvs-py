"""
  27. Faça um programa que leia o nome completo de uma pessoa, 
  mostrando em seguida oprimeiro
  e o último nome separadamente. Exemplo: Ana Maria de Souza, 
  primeiro: Ana, último: Souza
  
"""

# USANDO FOR

nome = input("Digite seu nome completo: ")

arrnome = nome.split()

pri_n = ""
ult_n = ""
for i in arrnome:
    pri_n = arrnome[0]
    ult_n = i

print(f"Seu nome completo é: {nome.title()} ")
print(f"Seu Primeiro Nome é: {pri_n.title()}") # arrnome[0]
print(f"Seu Ultimo Nome é: {ult_n.title()}")


# =============================================================================================

# USANDO WHILE

nome = input("Digite seu nome completo: ")

arrnome = nome.split()

pri_n = ""
ult_n = ""
i = 0
while(i < len(arrnome)):
    pri_n = arrnome[0]
    ult_n = arrnome[i]

    i = i + 1

print(f"Seu nome completo é: {nome.title()} ")
print(f"Seu Primeiro Nome é: {pri_n.title()}")
print(f"Seu Ultimo Nome é: {ult_n.title()}")
