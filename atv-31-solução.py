
"""
31. Desenvolva um programa que pergunte a distância de uma viagem em km.
Calcule opreço da passagem, cobrando 
R$0,50 por km para viagens de até 200km e R$0,45 paraviagens mais longas.
"""

distancia = float(input("Digite quantos km, tem a viagem: "))
valor = 0


if(distancia <= 200):
    valor = distancia * 0.50
    print(f"a distancia e {distancia}km e o valor a pagar pela viagem e R${valor}")
else:
    valor = distancia * 0.45
    print(f"a distancia e {distancia}km e o valor a pagar pela viagem e R${valor}")
    