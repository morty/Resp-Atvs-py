
"""
# 19. Um professor quer sortear um dos seus quatro alunos para apagar o quadro.
# Faça umprograma que ajude ele, lendo o nome deles e escrevendo o nome escolhido
"""


# RESPONDI A QUENSTAO 19 DE 3 FORMAS: UMA LIMITADA E FUNCIONAL, E OUTRAS Duas VERSATIL E FINCIONAL;

# OBS: De acordo com o enunciado, a 'resp 1' ja corresponde aos requisitos, mas achei interessante usar loop while e for;

# ================================================================================

# RESP 1

import random

# Dados de Entrada
aln1 = input("nome do primeiro aluno: ")
aln2 = input("nome do segundo aluno: ")
aln3 = input("nome do terceiro aluno: ")
aln4 = input("nome do quarto aluno: ")

# Execução
arr_alunos = [aln1, aln2, aln3, aln4]

# Saida
print(f"{ arr_alunos[random.randint(1, 4)]}")


# =================================================================================

# RESP 2

from random import randint

# Dados de entrada
arr_aln = [] # Array que vai receber os nomes dos alunos.
nomes = ""   # Para guardar temporario o nome de cada aluno
num_alun = int(input("quantos alunos deseja colocar para o sorteiro: ")) 

# Execução
i = 1 # o indice(i) e usado como nosso contador;
while(i <= num_alun): # Enquanto o indice(i), menor ou igual, ao numero de alunos, FAÇA!;
    nomes = input(f"nome do {i} aluno: ") # a variavel "nomes" recebera temporariamente o nome de um aluno;
    # !!ATENÇÃO!! estou a usar o termo ("temporario"), para variavel 'nomes',
    # porque a variavel 'nomes', a cada (loop/passada/repetição) vai ser sobrescrito
    # com outro nome.
    arr_aln.append(nomes) # Guardar o nome digitado em um (array/vetor) chamado 'arr_lan';

    i = i + 1 # Realizar os passos do contador
    # OBS: se voce nao colocar a incrementaçao seu codigo ira rodar infinitamente,
    # pois e graças ao encremento que o indice(i) consegue ultrapassar o numero da variavel 'num_alun'.

# Saída
print(f"O Aluno(a) sorteado(a) foi:  {arr_aln[randint(1, num_alun)]}")
# Aqui usei o "randint" para escolher um indice(i) aleatorio do (array/vetor) "arr_aln"
# e imprimir o nome do aluno, que esta no indice escolhido de forma aleatoria.

# =====================================================================================

# RESP 3

from random import randint

qtd = int(input("quantos alunos para sorteio? "))
arr = []

for n_aln in range(1, qtd+1):
    nome = input(f"nome do {n_aln} aluno: ")
    arr.append(nome)

print("o aluno sorteado foi", arr[randint(1, qtd)])

