# 7. Desenvolva um programa que leia as duas notas de um aluno e mostre a sua média.

# 8. Escreva um programa que leia um valor em metros e o exiba convertido em centímetrose milímetros.

# 9. Faça um programa que leia um número inteiro qualquer e mostre na tela a sua tabuada

#=================================================================

#RESP ATV 07

# Entrada de dados
nota1 = float(input("valor da Primeira nota: "))
nota2 = float(input("valor da segunda nota: "))

# Execução
media = (nota1 + nota2) / 2

# Saida de dados
print(f"a media foi do aluno foi {media}")

#===================================================================

#RESP ATV 08

# Entrada de dados
metros = float(input("Digíte valor de Metros: "))

# Execução
centimetros = (metros * 100)
milimetros = (metros * 1000)

# Saída de dados
print(f"convertendo {metros}metro(s) para milimetros e centimetros")
print(f"{metros}m = {centimetros}cm")
print(f"{metros}m = {milimetros}ml")


#======================================================================


#RESP ATV 09

# Entrada de dados
print("tabuada")
num = int(input("Digite um Numero: "))

# Execuçao e Saída
contador = 1
while(contador < 11):
    mult = num * contador
    print(f"{num} x {contador} = {mult}")
    contador += 1










