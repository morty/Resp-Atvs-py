# 4.Faça um programa que leia algo pelo teclado e mostre na tela o seu tipo primitivo e todas as informações possíveis sobre ele.

# 5.Faça um programa que leia um número inteiro e mostre na tela o seu sucessor e seu antecessor.

# 6.Crie um algoritmo que leia um número e mostre o seu dobro, triplo e raiz quadrada.


print("===============================================================")

# RESP da ATV 04

# Entrada
frase = input("digite qualquer frase: ")

# Execução/Saída
print("o tipo de dado na variavel e: ", type(frase))
print("a quantidade de caracteres da Frase são: ", len(frase))


print("==============================================================")

# RESP da ATV 05

print("esse programa mostra o antecessor e o sucessor")

# Entrada
numero = int(input("Digite um valor: "))

# Execução
antecessor = numero - 1   
sucessor = numero + 1

# Saída
print(f"O numero digitado foi {numero}")
print(f"O seu Antecessor e {antecessor}")
print(f"O seu Sucessor e {sucessor}")

print("=================================================================")

# RESP da ATV 06

# import math
# ou
from math import sqrt

# Entrada
num = int(input("Digite um Número: "))

# Execução
num1 = num * 2
num2 = num * 3
num3 = sqrt(num)

# Saída
print(f"O Dobro de {num} e {num1}")
print(f"O Triplo de {num} e {num2}")
print(f"A Raiz Quadrada de {num} e {round(num3, 2)}")

print("===============================================================")