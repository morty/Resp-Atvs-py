 26.Faça um programa que leia uma frase pelo tevlado e mostre:
  => (a) Quantas vezes aparece a letra "A".
  => (b) Em que posição ela aparece pela primeira vez.
  => (c) Em que posição ela aparece pela última vez


=========================== LOOP WHILE ======================================
frase = input("Digitalize uma Frase: ")
cont_a = 0
i = 0
while(i < len(frase)):
  if(frase[i] == "a" or frase[i] == "A"):
       cont_a += 1
  i += 1

pri_a = 0
x = 0
while(x < len(frase)):
  if(frase[x] == "a" or frase[x] == "A"):
       pri_a = x
       break
  x += 1

ult_a = 0
n = 0
while(n < len(frase)):
    if(frase[n] == "a" or frase[n] == "A"):
       ult_a = n
    n += 1

print(f"A letra 'A' se Repete {cont_a} vezes.")
print(f"A primeira letra 'A', se encontra na posição: {pri_a+1}")
print(f"A ultima letra 'A', se encontra na posição: {ult_a+1}")
====================================================================================



=========================== LOOP FOR =======================================
frase = input("Digitalize uma Frase: ")
ult_a = 0
cont_a = 0
pri_a = 0


for i in frase:
     if(i == "a" or i == "A"):
         cont_a += 1

o = 1
for x in frase:
   if(x == "a" or x == "A"):
       pri_a = o
       break
   o += 1

k = 0
for n in frase:
    if (n == "a" or n == "A"):
        ult_a = k

    k += 1

print(f"A letra 'A' se Repete {cont_a} vezes.")
print(f"A primeira letra 'A', se encontra na posição: {pri_a}")
print(f"A ultima letra 'A', se encontra na posição: {ult_a+1}")
====================================================================================