# 21. Faça um programa em Python que abra e reproduza o áudio de um arquivo MP3

# Existem diversas formas de reproduzir um audio, aqui mostrarei só duas;
import pygame
pygame.init()
pygame.mixer.music.load("som.mp3") # copie e cole o arquivo 'mp3' no pycharm e use somente os nome
pygame.mixer.music.play()
pygame.event.wait()

import playsound
playsound.playsound(f"/home/usuario/Downloads/som.mp3") # aqui e o caminho do sistema operacional linux.

# voce vai colocar
# todo o caminho que sera feito pelo seu diretorios
# ate chegar na pasta de origem do "audio" e assim tocalo, ou
# voce pode, colocalo no 'pycharm' em formato de 'arquivo.mp3'
# e so utilizar direto.
