# 10. Crie um programa que leia quanto dinheiro uma pessoas tem na carteira
# e mostre quantos dólares ela pode comprar. Considere U$$1,00 = R$3,37.

# 11. Faça um programa que leia a largura e a altura de uma parede em metros,
# calcule a sua área e a quantidade de tinta necessária para pintá-la,
# sabendo que cada litro de tintapinta uma área de 2m².

# 12. Faça um algoritmo que leia o preço de um produto e mostre seu novo preço, com 5% dedesconto.

# 13. Faça um algoritmo que leia o salário de um funcionário
# e mostre seu novo salário, com 15% de aumento.

#=====================================================================================

# RESP ATV 10

# Entrada de dados
carteira_da_pessoa = float(input("qunato saldo disponivel na carteira: "))
# Execução
dolares_compra = carteira_da_pessoa / 3.37
# Saída de dados
print(f"Com o saldo de R${carteira_da_pessoa}, voce pode comprar U$${round(dolares_compra, 2)}")

#=====================================================================================

# RESP ATV 11

# Entrada de dados
largura = int(input("Quantos metros de largura, tem sua parede? "))
altura = int(input("Quantos metros de altura, tem sua parede? "))

# Execução
area = (largura * altura)
tinta_necessaria =  area / 2

# Saída de dados
print(f"Para tantos {area}m² de parede voce vai precisar de {tinta_necessaria} Litros de tinta")

#=====================================================================================

# RESP ATV 12

# Entrada de dados
prec_produto = float(input("Preço do Produto: "))

# Execução
prec_desc = prec_produto - ((prec_produto * 5) / 100)

# Saída de dados
print(f"Preço Anterior: R${prec_produto}, e seu Preço agora é: R${round(prec_desc, 2)}")

#=====================================================================================

# RESP ATV 13

# Entrada de dados
salario = float(input("Salario do Funcionario: "))
# Execução
salario_aumento = salario + ((salario * 15) / 100)
# Saída
print(f"o salario do funcionario era de R${salario} mas com o aumento de 15% ficou R${salario_aumento}")
