"""
 14. Escreva um programa que converta uma
 temperatura digitada em C° e converta para F°.

 15. Escreva um programa que pergunte a quantidade de km percorridos
 por um carro alugado e a quantidade de dias pelos quais ele foi alugado.
 Calcule o preço a pagar, sabendo que o carro custa R$60 por dia e R$0,15 por km rodado.

 16. Crie um programa que leia um número real qualquer pelo teclado
 e mostre na tela a sua porção inteira. Exemplo: o número 6,127 tem a parte inteira 6.

 17. Faça um programa que leia o comprimento do cateto oposto
 e do cateto adjacente de um
 triângulo retângulo, calcule e mostre o comprimento da hipotenusa.

 18. Faça um programa que leia um ângulo qualquer e mostre na tela o valor do seno,
 cosseno e tangente desse ângulo.

"""
# =================================================================================

# variaveis: reais -> (celcius,fareheit);
# RESP ATV 14

print("Convertendo Celcius para Farehint")
# Dados de Entrada
celcius = float(input("Digite os Celcius: "))
# Execução
fareheit = (celcius * (9/5)) + 32
# Saída de dados
print("convertendo {0}° celcius para {1}° de fareheit".format(celcius, fareheit))

# =================================================================================

# RESP ATV 15
# variaveis int(dias), float(km_percorridos, preco_tot)
print("Alugel de Carro ou Carro Alugado")

# Dados de entrada
km_percorridos = float(input("Quantos kilometros foram Percorridos: "))
dias_alugado = int(input("Por Quantos dias o Carro Foi Alugado: "))
# Execução
preco_tot = (km_percorridos * 0.15) + (dias_alugado * 60)
# Saida
print(f"o carro foi alugado por {dias_alugado} dia(s) ")
print(f" Percorreu {km_percorridos}km")
print(f"valor total a se pagar R${preco_tot}")

# ==============================================================================

# RESP ATV 16

# Entrada de dados
num_real = float(input("digite qualquer numero real com ponto flutuate ou virgula: "))
# Execução
num_inteiro = int(num_real)
# Saída
print(f"o numero real {num_real} convertido para proporção inteira fica: {num_inteiro}")

# ===================================================================

# RESP ATV 17
from math import sqrt

print("Calculando a Hipotenusa")

# Entrada de dados
cat_adijacente = float(input("informe o valor numerico do cateto adjacente: "))
cat_oposto = float(input("Informe o valor numerico de cateto oposto: "))

# formula para calcular a hipotenusa: c² = a² + b² 
# Execução
hipot = (cat_adijacente**2) + (cat_oposto**2)
hipotenusa_tot = sqrt(hipot)

# Saída
print(f"o valor da hipotenusa e {hipotenusa_tot}")

# =================================================================================

# RESP ATV 18

# Dados de entrada
grau = int(input("qual o valor do grau(0°, 30, 45°, 60° ou 90°): "))

# Execução/Saída
if grau == 0:
    print("seno = 0")
    print("cossseno = 1")
    print("tangente = 0")

elif grau == 30:
    print("seno = 1/2")
    print("cossseno =  √3/2")
    print("tangente =  √3/3")

elif grau == 45:
    print("seno =  √2/2")
    print("cossseno =  √2/2")
    print("tangente = 1")

elif grau == 60:
    print("seno = √3/2")
    print("cossseno = 1/2")
    print("tangente =  √3")

elif grau == 90:
    print("seno = 1")
    print("cossseno = 0")
    print("tangente = ∞")

else:
    print("grau nao valido.")
