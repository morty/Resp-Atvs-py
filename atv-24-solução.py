"""
24. Crie um programa que leia o nome 
de uma cidade e diga se ela começa ou não com onome "SANTO"
"""
#========================== LOOP WHILE ======================================

cidade = input("Digite o nome de sua cidade: ")

pri_n = ""
i = 0
while(i < len(cidade)):
    if(cidade[i] == " "):
        break
    else:
        pri_n += cidade[i]
    i += 1

nome_ci = pri_n.upper() # convertendo tudo para maiusculo para nao ter problemas de condiçao;

if(nome_ci == "SANTO"):
    print("o nome se sua cidade começa com Santo")
else:
    print("o nome de sua cidade nao começa com santo")

print(f"o nome completo de sua cidade e: {cidade.title()}")


#================================= LOOP FOR =================================

cidade = input("Digite o nome de sua cidade: ")

pri_n = ""
for pri in cidade:
    if(pri == " "):
        break
    else:
        pri_n += pri
        
nome_ci = pri_n.upper()

if(nome_ci == "SANTO"):
    print("o nome se sua cidade começa com Santo")
else:
    print("o nome de sua cidade nao começa com santo")

print(f"o nome completo de sua cidade e: {cidade.title()}")
