


# ========================== UTILIZANDO O LOOP WHILE =================================

# Entrada de dados
qtd = int(input("Quantos valores deseja guardar: "))
numero = []
maior = -99999
menor = 99999

# Execução
i = 0
while (i < qtd):
    numero.append(int(input(f"digite o {i+1} valor em inteiro: ")))

    i = i + 1

contador = 0
while(contador < len(numero)):
    if(maior < numero[contador]):
         maior = numero[contador]
         
    if(menor > numero[contador]):
        menor = numero[contador]

    contador = contador + 1

# Saída
print(f"maior valor: {maior}")
print(f"menor valor: {menor}")






# ======================= UTILIZANDO LOOP FOR ===================================

# Entrada de dados
numeros = []
min = 99999999
max = -99999999

# Execução
for c in range(0, 5):
    numeros.append(int(input(f"Digite o {c+1} valor: ")))

    if numeros[c] > max:
         max = numeros[c]

    if numeros[c] < min:
         min = numeros[c]

#  Saída
print(f"o maior valor {max}")
print(f"o menor valor {min}")


