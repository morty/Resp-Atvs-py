"""
30. Crie um programa que leia um número
 inteiro e mostre na tela se ele 
 é PAR ou ÍMPAR
"""



num = int(input("Digite qualquer número: "))

if ((num%2) == 0):
     print(f"o número {num} é PAR")
else:
     print(f"o número {num} é IMPAR")
