"""
22. Crie um programa que leia o nome completo de uma pessoa e mostre:
  (a) O nome com todas as letras maiúsculas e minúsculas.
  (b) Quantas letras ao todo (sem considerar espaços)
  (c) Quantas letras tem o primeiro nome.
"""

# ========================== USANDO O LOOP FOR ================================================

nome = input("Digite seu Nome Completo: ")
quant_caracter = int(len(nome)) # so serve para contar caracteres (sem o espaço)
pri_nome = ""

# VAI VERIFICAR SE TEM ESPAÇOS E IRA REMOVELOS!
for caracter in nome: 
   if caracter == " " or caracter == "  ":
       quant_caracter -= 1

# VAI CONCATENAR AS STRINGS ATE QUE O 'INDICE' SEJA VAZIO, AI VAI SAIR NO "BREAK"!:
for indice in nome:
     if indice == " ":
        break
     else:
        pri_nome += indice     #"+j+o+a+o"

# (A)
print(f"Seu nome em Maiusculo fica: {nome.upper()}")
print(f"Seu nome em Minusculo fica: {nome.lower()}")

# (B)
print(f"A quantidade de carcteres(sem contar com espaços) em seu nome é: {quant_caracter}")
print(f"A quantidade de carcteres(contando com espaços) em seu nome é: {len(nome)}")

# (C)
print(f"O seu Primeiro Nome é: {pri_nome.title()}")
print(f"A Quantidade de Caracteres do seu Primeiro nome é: {len(pri_nome)}")





# =============================== USANDO O LOOP 'WHILE' ========================================

nome = input("Digite seu Nome Completo: ")
quant_caracter = int(len(nome)) # so serve para contar caracters (sem o espaço)
pri_nome = ""


c = 0
while(c < len(nome)):
    if nome[c] == " " or nome[c] == "  ":
        quant_caracter -= 1

    c += 1

p = 0
while(p < len(nome)):
    if nome[p] == " ":
        break
    else:
        pri_nome += nome[p]

    p += 1


# (A)
print(f"Seu nome em Maiusculo fica: {nome.upper()}")
print(f"Seu nome em Minusculo fica: {nome.lower()}")

# (B)
print(f"A quantidade de carcteres(sem contar com espaços) em seu nome é: {quant_caracter}")
print(f"A quantidade de carcteres(contando com espaços) em seu nome é: {len(nome)}")

# (C)
print(f"O seu Primeiro Nome é: {pri_nome.title()}")
print(f"A Quantidade de Caracteres do seu Primeiro nome é: {len(pri_nome)}")

