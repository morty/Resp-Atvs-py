# 20. O mesmo professor do exercício anterior quer
# sortear a ordem de apresentação de trabalhos dos alunos.
# Faça um programa que leia o nome dos alunos e mostre a ordem sorteada.


#======================== UTILIZANDO O LOOP FOR ====================================================

from random import shuffle

# Etrada de dados
qtd = int(input("Quantos Alunos serão Sorteados: "))
nomes = []

# Execição
for aln in range(0, qtd): # para cada 'aln' de '0' ate 'qtd(escolhida)' Faça;
       nomes.append(input(f"nome do {aln+1}: ")) # vai adicionar os cada nome de aluno dentro do array 'nomes'


shuffle(nomes) # vai embaralhar o array 'nomes' 


# Saida
print("\n==========================================")
print("  ORDEM PARA APRESENTAÇÃO DOS TRABALHOS  ")
print("==========================================")

# Saída
cont = 0
for i in nomes:
    print(f"{cont+1}.{i}")
    cont += 1

print("==========================================")

#====================================================================================================



#============================= UTILIZANDO O LOOP WHILE ==============================================

from random import shuffle

qtd = int(input("quantos alunos serao sorteado: "))
nome = []

i = 0
while(i < qtd):
    nome.append(input(f"qual o nome do {i+1} aluno: "))
    i += 1

shuffle(nome)

print("\n==========================================")
print("  ORDEM PARA APRESENTAÇÃO DOS TRABALHOS  ")
print("==========================================")

num = 1
cont = 0
while(cont < len(nome)):
    print(f"{num}.{nome[cont]}")

    num += 1
    cont += 1

print("==========================================")