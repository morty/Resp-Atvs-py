
"""
 29.Escreva um programa que leia a velocidade de um carro.
 Se ele ultrapassar 80km/h,mostre uma mensagem dizendo que ele foi multado.
 A multa vai custar R$7,00 por cdakm acima do limite
"""


# ================ USANDO O LOOP WHILE =============================

vel_carro = int(input("Digite a velocidade do carro: "))

# usei este loop para calcular a velocidade acima de 80km/s
vel_acima = 0

while(vel_carro > 0):
     if(vel_carro > 80):
          vel_acima += 1 # guarda cada vaclor acima de 80km/s
     vel_carro -= 1

val_multa = (vel_acima * 7)  # calculando a multa

if(val_multa == 0):
    print("tudo certo siga tranquilo")
else:
    print(f"voce foi multado por passar a velocidade limite de 80km/s, multa: R${val_multa}")


# =============================================================================================

# ============== USANDO LOOP FOR ==================================

vel_acima = 0

for vel in range(vel_carro, 0, -1):
    if (vel > 80):
        vel_acima += 1

val_multa = (vel_acima * 7)  # calculando a multa

if(val_multa == 0):
    print("tudo certo siga tranquilo")
else:
    print(f"voce foi multado por passar a velocidade limite de 80km/s, multa: R${val_multa}")

